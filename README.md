# Projact Info

Task manager (shcool job #04)

# Developr Info

**NAME:** Maxim Bakanov

**E-MAIL:** dragonnirvald@gmail.com

# Software 

- JDK 1.8
- MS Windows 10

# Hardware 

**CPU:** Intel i7-4770 or highter

**RAM:** 16 Gb DDR3-2400MHz or highter

**ROM:** 500 Gb M2.NVMe SSD or highter

# Programm run

`java -jar ./task-manager-jse.jar`

### Another files

[screenshots](https://drive.google.com/drive/folders/1wnLx68cynwgrYrHcpVs4w7AH5qZMg4fv?usp=sharing)

